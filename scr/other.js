var counter = 60;
var timer;
var tmp;
var flag = true;
var player1;
var player2;
var player3;
var save;
 
function setElem() {
    if (tmp == 1) {
        elem3 = document.getElementById('container1');
        elem2 = document.getElementById('container2');
        elem1 = document.getElementById('container3');
    } else if (tmp == 2) {
        elem3 = document.getElementById('container2');
        elem2 = document.getElementById('container3');
        elem1 = document.getElementById('container1');
    } else {
        elem3 = document.getElementById('container3');
        elem2 = document.getElementById('container2');
        elem1 = document.getElementById('container1');
    }
}

function start() {
    timer = setInterval(clock, 1000);

    function clock() {
        counter--;

        if (counter <= 0) {
            clearInterval(timer);
            $('#time').text("Time out");
            endList();
        } else {
            //$('#time').text(counter);
            document.getElementById("time").innerHTML = counter;
        }
    }
}

function firstOut() {
    gameStop();
    clearInterval(timer);

    document.getElementById("result-block").style.display = "block";
    document.getElementById("game-area").style.backgroundColor = "red";

    document.getElementById("container1").style.display = "none";
    document.getElementById("container2").style.display = "none";
    document.getElementById("container3").style.display = "none";

    document.getElementById("winner").innerHTML = player3;
    document.getElementById("loser").innerHTML = player1;
    document.getElementById("time-left").innerHTML = counter;
}

function secondOut() {
    gameStop();
    clearInterval(timer);

    document.getElementById("result-block").style.display = "block";
    document.getElementById("game-area").style.backgroundColor = "red";

    document.getElementById("container1").style.display = "none";
    document.getElementById("container2").style.display = "none";
    document.getElementById("container3").style.display = "none";

    document.getElementById("winner").innerHTML = player3;
    document.getElementById("loser").innerHTML = player2;
    document.getElementById("time-left").innerHTML = counter;
}

function endList() {
    gameStop();
    clearInterval(timer);

    document.getElementById("result-block").style.display = "block";
    document.getElementById("game-area").style.backgroundColor = "red";

    document.getElementById("container1").style.display = "none";
    document.getElementById("container2").style.display = "none";
    document.getElementById("container3").style.display = "none";

    if (count1 < count2) {
        document.getElementById("winner").innerHTML = player1;
        document.getElementById("loser").innerHTML = player3;
    } else if (count2 < count1) {
        document.getElementById("winner").innerHTML = player2;
        document.getElementById("loser").innerHTML = player3;
    } else {
        document.getElementById("winner").innerHTML = "Both";
        document.getElementById("loser").innerHTML = player3;
    }
}

var player = 2;

function setName() {
    var name = document.getElementById("name").value;

    if (player == 2) {
        player1 = name;

        $('#name1').text(name);
        $('#player1').text(name);

        $('#input-name').text("Enter player2's name");
    } else if (player == 3) {
        if (name == player1) {
            player--;
            alert("Change another name.");
        } else {
            player2 = name;

            $('#name2').text(name);
            $('#player2').text(name);

            $('#input-name').text("Enter player3's name");
        }
    } else {
        if (name == player1 || name == player2) {
            player--;
            alert("Change another name.");
        } else {
            player3 = name;

            $('#name3').text(name);
            $('#player3').text(name);

            document.getElementById("input-block").style.display = "none";

            tmp = Math.floor(Math.random() * 3) + 1;

            if (tmp == 1) {
                var el = document.getElementById("character1");

                $('#player1').text(player3);
                $('#player3').text(player1);

                elem3 = document.getElementById('container1');
                elem2 = document.getElementById('container2');
                elem1 = document.getElementById('container3');

                save = player3;
                player3 = player1;
                player1 = save;
            } else if (tmp == 2) {
                var el = document.getElementById("character2");

                $('#player2').text(player3);
                $('#player3').text(player2);

                elem3 = document.getElementById('container2');
                elem2 = document.getElementById('container3');
                elem1 = document.getElementById('container1');

                save = player3;
                player3 = player2;
                player2 = save;
            } else {
                var el = document.getElementById("character3");

                elem3 = document.getElementById('container3');
                elem2 = document.getElementById('container2');
                elem1 = document.getElementById('container1');
            }

            el.setAttribute("src", "img/ghost.png");
        }
    }

    document.getElementById("name").value = "";
    player++;
}

function callName() {
    $('#name1').text("Player1");
    $('#name2').text("Player2");
    $('#name3').text("Player3");
    $('#player1').text("Player1");
    $('#player2').text("Player2");
    $('#player3').text("Player3");
    $('#count1').text("count:");
    $('#count2').text("count:");
    $('#count3').text("count:");
    document.getElementById("container1").style.display = "block";
    document.getElementById("container2").style.display = "block";
    document.getElementById("container3").style.display = "block";

    player = 2;
    count1 = 0;
    count2 = 0;
    counter = 60;

    clearInterval(timer);
    $('#time').text("60");
    $('#input-name').text("Enter player1's name");
    document.getElementById("input-block").style.display = "block";
    document.getElementById("result-block").style.display = "none";
    document.getElementById("game-area").style.backgroundColor = "rgb(245, 131, 65)";

    var el = document.getElementById("character1");
    el.setAttribute("src", "img/player1.png");
    el = document.getElementById("character2");
    el.setAttribute("src", "img/player2.png");
    el = document.getElementById("character3");
    el.setAttribute("src", "img/player3.png");

    window.document.onkeydown = null;
}
